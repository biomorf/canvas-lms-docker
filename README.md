# Docker Image for Canvas LMS

This docker image of Canvas LMS builds the application in one single container. It still relies on external services for database and caching.

You can run multiple instances of this container for web and bg worker tasks.



## Build

    $ docker build -t ${ACCOUNT}/canvas-lms:${TAG} .

## Install

Update `docker-compose.yml` with correct ENV_VARS

### DB Intial setup

    $ docker-compose up -d postgres redis
    $ docker-compose run --rm db_initial

### Run with canvas

    $ docker-compose up -d postgres redis minio web

### Setup a bucket on minio

    $ docker-compose up -d minio
Navigate to the minio UI and create a bucket with the same name you set in `S3_BUCKET_NAME`.

### Reset Encryption Key

    $ docker-compose run --rm reset_encryption_key

### SSH to container

    $ docker-compose run --rm bash
